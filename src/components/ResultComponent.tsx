import * as React from "react";
import Container from "react-bootstrap/Container";


type ResultProps = {
    display: string;
};


class ResultComponent extends React.Component<ResultProps, {}>
{
    static defaultProps: ResultProps = {
        display: "0",
    }

    render()
    {
        let display = this.props.display;
        if (display === "")
        {
            display = "0";
        }

        return (
            <Container>
                <h1 className="m-3 font-weight-bold text-right">
                    {display}
                </h1>
            </Container>
        );
    }
}


export default ResultComponent;
