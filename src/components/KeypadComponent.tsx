import * as React from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

import App from "../App";
import { AppState } from "../App";


type KeyProp = {
    label: string;
    callback: (callback: (state: AppState) => AppState) => void;
};


type KeypadProp = {
    callback: (callback: (state: AppState) => AppState) => void;
};


function doOp(x: number | string, y: number | string, op: string): number
{
    x = Number(x);
    y = Number(y);

    switch (op)
    {
    case "+":
        return y + x;

    case "-":
        return y - x;

    case "*":
        return y * x;

    case "/":
        return y / x;

    default:
        return y;
    }
}


abstract class KeyComponent extends React.Component<KeyProp>
{
    render()
    {
        let style = {
            borderRadius: "0px",
            fontSize: "20px",
        };

        return (
            <Button
                style={style}
                onClick={e => this.props.callback(this.updateState)}
            >
                {this.props.label}
            </Button>
        );
    }

    public abstract updateState(state: AppState): AppState;
}


class NumberKeyComponent extends KeyComponent
{
    constructor(props: KeyProp)
    {
        super(props);
        this.updateState = this.updateState.bind(this);
    }

    public updateState(state: AppState): AppState
    {
        let nextState = Object.assign({}, state);
        let key = this.props.label;
        let x = nextState.x;

        if (state.shift)
        {
            nextState = Object.assign({}, nextState, {
                x: "",
                y: x,
                shift: false,
            });
            x = "";
        }

        if (key === "." && x.includes("."))
        {
            return state;
        }

        x = x + key;
        nextState.x = x;
        return nextState;
    }
}


class OperatorKeyComponent extends KeyComponent
{
    constructor(props: KeyProp)
    {
        super(props);
        this.updateState = this.updateState.bind(this);
    }

    public updateState(state: AppState): AppState
    {
        let nextState = Object.assign({}, state);
        if (nextState.y !== "")
        {
            nextState = Object.assign({}, nextState, {
                x: doOp(nextState.x, nextState.y, nextState.op).toString(),
            });
        }

        return Object.assign({}, nextState, {
            op: this.props.label,
            shift: true,
        })
    }
}


class EqualKeyComponent extends KeyComponent
{
    constructor(props: KeyProp)
    {
        super(props);
        this.updateState = this.updateState.bind(this);
    }

    public updateState(state: AppState): AppState
    {
        return Object.assign({}, state, {
            x: doOp(state.x, state.y, state.op).toString(),
        });
    }
}


class ClearKeyComponent extends KeyComponent
{
    constructor(props: KeyProp)
    {
        super(props);
        this.updateState = this.updateState.bind(this);
    }

    public updateState(state: AppState): AppState
    {
        return App.defaultState;
    }
}


class KeypadComponent extends React.Component<KeypadProp>
{
    render()
    {
        return (
            <Container fluid className="d-flex flex-column bg-primary">
                <Row className="flex-grow-1" xl={4} lg={4} md={4} sm={4} xs={4}>
                    <ClearKeyComponent label={"AC"} callback={this.props.callback}/>
                </Row>
                <Row className="flex-grow-1" xl={4} lg={4} md={4} sm={4} xs={4}>
                    <NumberKeyComponent label={"7"} callback={this.props.callback}/>
                    <NumberKeyComponent label={"8"} callback={this.props.callback}/>
                    <NumberKeyComponent label={"9"} callback={this.props.callback}/>
                    <OperatorKeyComponent label={"/"} callback={this.props.callback}/>
                </Row>
                <Row className="flex-grow-1" xl={4} lg={4} md={4} sm={4} xs={4}>
                    <NumberKeyComponent label={"4"} callback={this.props.callback}/>
                    <NumberKeyComponent label={"5"} callback={this.props.callback}/>
                    <NumberKeyComponent label={"6"} callback={this.props.callback}/>
                    <OperatorKeyComponent label={"*"} callback={this.props.callback}/>
                </Row>
                <Row className="flex-grow-1" xl={4} lg={4} md={4} sm={4} xs={4}>
                    <NumberKeyComponent label={"1"} callback={this.props.callback}/>
                    <NumberKeyComponent label={"2"} callback={this.props.callback}/>
                    <NumberKeyComponent label={"3"} callback={this.props.callback}/>
                    <OperatorKeyComponent label={"-"} callback={this.props.callback}/>
                </Row>
                <Row className="flex-grow-1" xl={4} lg={4} md={4} sm={4} xs={4}>
                    <NumberKeyComponent label={"0"} callback={this.props.callback}/>
                    <NumberKeyComponent label={"."} callback={this.props.callback}/>
                    <EqualKeyComponent label={"="} callback={this.props.callback}/>
                    <OperatorKeyComponent label={"+"} callback={this.props.callback}/>
                </Row>
            </Container>
        );
    }
}


export default KeypadComponent;
