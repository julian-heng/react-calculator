import React from "react";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

import ResultComponent from "./components/ResultComponent";
import KeypadComponent from "./components/KeypadComponent";

import "bootstrap/dist/css/bootstrap.min.css";


export type AppState = {
    x: string;
    y: string;
    op: string;
    shift: boolean;
};


class App extends React.Component<{}, AppState>
{
    static defaultState: AppState = {
        x: "",
        y: "",
        op: "",
        shift: false,
    }

    constructor(props: Object)
    {
        super(props);
        this.state = App.defaultState;
        this.keyCallback = this.keyCallback.bind(this);
    }

    render()
    {
        return (
            <Container fluid className="d-flex flex-column vh-100">
                <Row>
                    <ResultComponent display={this.state.x}/>
                </Row>
                <Row className="flex-grow-1">
                    <KeypadComponent callback={this.keyCallback}/>
                </Row>
            </Container>
        );
    }

    public keyCallback(callback: (state: AppState) => AppState): void
    {
        this.setState(callback(this.state));
    }
}


export default App;
